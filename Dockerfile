FROM ubuntu:16.04

RUN apt-get update \
  && apt-get install -qqy \
     virtualenv openssh-client git sudo python python-pip postgresql python-psycopg2 libpq-dev python-m2crypto gettext \
  && apt-get -yqq autoremove \
  && apt-get -y clean \
  && rm -rf /var/lib/apt/lists/*

